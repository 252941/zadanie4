#include <iostream>
#include "Wektor.hh"
#include "Macierz.hh"
#include "UkladRownanLiniowych.hh"
#include "LZespolona.hh"


using namespace std;


int main()
{
  char r_z;
  cin >> r_z;
  if (r_z == 'r')
  {
  double liczby[ROZMIAR+1][ROZMIAR]; // [numer wiersza][numer kolumny]

  for(int i = 0; i < ROZMIAR+1; i++)
  {
    for(int j = 0; j < ROZMIAR; j++)
    {
      cin >> liczby[i][j];
    }
  }

  SMacierz<double, ROZMIAR> mac;
  for(int i = 0; i < ROZMIAR; i++)
  {
    mac.wczytajKolumne(i, liczby[i]);
  }

  SWektor<double, ROZMIAR> wyr_wolne(liczby[ROZMIAR]);

  SUkladRownanLiniowych<double, ROZMIAR> url(mac,wyr_wolne);
  cout << "Rozwiazanie x = (x1, x2, x3):" << url.podajWynikCramer() << endl;
  //cout << "Dlugosc wektora bledu: " << url.wielkoscBledu() << endl << endl;
  }
  else if (r_z == 'z')
  {
LZespolona liczby[ROZMIAR+1][ROZMIAR]; // [numer wiersza][numer kolumny]

  for(int i = 0; i < ROZMIAR+1; i++)
  {
    for(int j = 0; j < ROZMIAR; j++)
    {
      cin >> liczby[i][j];
    }
  }

  SMacierz<LZespolona, ROZMIAR> mac;
  for(int i = 0; i < ROZMIAR; i++)
  {
    mac.wczytajKolumne(i, liczby[i]);
  }

  SWektor<LZespolona, ROZMIAR> wyr_wolne(liczby[ROZMIAR]);

  SUkladRownanLiniowych<LZespolona, ROZMIAR> url(mac,wyr_wolne);
  cout << "Rozwiazanie x = (x1, x2, x3):" << url.podajWynikCramer() << endl;
  //cout << "Dlugosc wektora bledu: " << url.wielkoscBledu() << endl << endl;
  }
  else 
  {
    cout << "Nie ma takiej opcji.Dostepne opcje to r-liczby rzeczywiste, z-liczby zespolone" << endl;
  
  }
}
/*
  double t[3] = {1,-3,6};
  double t2[3] = {3,1,8};
  double t3[3] = {4,5,91};

  double t4[3] = {6,2.6,-0.01};

  Wektor w1(t);
  Wektor w2(t2);
  Wektor w3(t3);

  Macierz m;
  m.wczytajWiersz(0,t);
  m.wczytajWiersz(1,t2);
  m.wczytajWiersz(2,t3);

  cout << m.det();

  Wektor w_w(t4);

  UkladRownanLiniowych   UklRown(m,w_w);

  Wektor wynik = UklRown.podajWynikCramer();

  cout << "Rozwiazanie ukladu to " << wynik << endl;

  Wektor wynik_mnozenia = m*wynik;

  std::cout << "Wynik mnozenia macierzy z wektorem: " << wynik_mnozenia << endl;

  std::cout << "Sprawdzenie: " << (wynik_mnozenia)-w_w << endl;

  cout << endl << " Start programu " << endl << endl;
}
*/