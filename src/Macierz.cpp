/*#include "Macierz.hh"
#include "rozmiar.h"

using namespace std;

Wektor& Macierz::pobierzWiersz(int q)
{
    return tab[q];
}

double& Macierz::pobierzWyraz(int wiersz, int kolumna)
{
    return pobierzWiersz(wiersz).pobierz(kolumna);
}

Wektor& Macierz::pobierzKolumne(int q)
{
    SWektor wynik;
    
    for(int j = 0; j < ROZMIAR; j++)
    {
        wynik.wczytaj(j, tab[j].pobierz(q) );
    }

    return wynik;
}

void Macierz::wczytajKolumne(int p, Wektor q)
{
    for(int j = 0; j < ROZMIAR; j++)
    {
        tab[j].wczytaj(p,q.pobierz(j));
    }
}

void Macierz::wczytajWiersz(int p, Wektor q)
{
    tab[p]=q;
}

void Macierz::wczytaj(int wiersz, int kolumna, double wartosc)
{
    tab[wiersz].wczytaj(kolumna,wartosc);
}

ostream& operator << (ostream &wyjscie, Macierz &maci)
{
    for(int j=0;j<ROZMIAR;j++)
    {
        wyjscie << maci.pobierzWiersz(j) << " ";
    }
    return wyjscie;
}
Macierz operator* (Macierz m1,Macierz m2)
{
    Macierz mf;
    for(int j=0;j<ROZMIAR;j++)
    {
        for(int k = 0; k < ROZMIAR; k++)
        {
            int wartosc_iloczynu_skalarnego = m1.pobierzWiersz(j)*m2.pobierzKolumne(k);
            mf.wczytaj(j,k,wartosc_iloczynu_skalarnego);
        }
    }
    return mf;
}

    // 3 2 1 = tab[0]
    // 5 6 3 = tab[1]
    // 1 2 3 = tab[2]

double Macierz::det()
{
   // Macierz& m = (*this);
    // return m[0][0]*m[1][1]*m[2][2]+m...
    return pobierzWyraz(0,0)*pobierzWyraz(1,1)*pobierzWyraz(2,2)+pobierzWyraz(0,1)*pobierzWyraz(1,2)*pobierzWyraz(2,0)+pobierzWyraz(0,2)*pobierzWyraz(1,0)*pobierzWyraz(2,1)-pobierzWyraz(2,0)*pobierzWyraz(1,1)*pobierzWyraz(0,2)-pobierzWyraz(2,1)*pobierzWyraz(1,2)*pobierzWyraz(0,0)-pobierzWyraz(2,2)*pobierzWyraz(1,0)*pobierzWyraz(0,1);
}

Wektor operator*(SMacierz<STyp,SWymiar> m, SWektor<STyp,SWymiar> w)
{
    Wektor wynik;
    for(int i = 0; i < ROZMIAR; i++)
    {
        wynik.wczytaj(i, m.pobierzWiersz(i)*w);
    }
    return wynik;
}


Macierz Macierz::MacierzDopelnien()
{
    Macierz mf;
    int a,b,c,d,e,f,g,h,i;
    a=pobierzWyraz(1,1)*pobierzWyraz(2,2)-pobierzWyraz(2,1)*pobierzWyraz(1,2);
    b=(-1)*pobierzWyraz(1,0)*pobierzWyraz(2,2)-pobierzWyraz(2,0)*pobierzWyraz(1,2);
    c=pobierzWyraz(1,0)*pobierzWyraz(2,1)-pobierzWyraz(2,0)*pobierzWyraz(1,1);
    d=(-1)*pobierzWyraz(0,1)*pobierzWyraz(2,2)-pobierzWyraz(2,1)*pobierzWyraz(0,2);
    e=pobierzWyraz(0,0)*pobierzWyraz(2,2)-pobierzWyraz(2,0)*pobierzWyraz(0,2);
    f=(-1)*pobierzWyraz(0,0)*pobierzWyraz(2,1)-pobierzWyraz(2,0)*pobierzWyraz(0,1);
    g=pobierzWyraz(0,1)*pobierzWyraz(1,2)-pobierzWyraz(1,1)*pobierzWyraz(0,2);
    h=(-1)*pobierzWyraz(0,0)*pobierzWyraz(1,2)-pobierzWyraz(1,0)*pobierzWyraz(0,2);
    i=pobierzWyraz(0,0)*pobierzWyraz(1,1)-pobierzWyraz(1,0)*pobierzWyraz(0,1);
    mf.wczytaj(0,0,a);
    mf.wczytaj(0,1,b);
    mf.wczytaj(0,2,c);
    mf.wczytaj(1,0,d);
    mf.wczytaj(1,1,e);
    mf.wczytaj(1,2,f);
    mf.wczytaj(2,0,g);
    mf.wczytaj(2,1,h);
    mf.wczytaj(2,2,i);

    return mf;
}
Macierz Macierz::MacierzTranspozycja()
{
    Macierz mf;

}*/