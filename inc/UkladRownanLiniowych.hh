#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include <iostream>
#include <cmath>
#include "Macierz.hh"
#include "Wektor.hh"


/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
template <typename STyp, int SWymiar>
class SUkladRownanLiniowych {
  SMacierz<STyp, SWymiar> wsp;
  SWektor<STyp, SWymiar> wyr_wol;
  public: 
  SUkladRownanLiniowych(SMacierz<STyp, SWymiar> m, SWektor<STyp, SWymiar> w)
  {
    wsp = m;
    wyr_wol = w;
  }
  SWektor<STyp,SWymiar> podajWynikCramer()
  {
    STyp wx[SWymiar];
    STyp w = wsp.det();

    SMacierz<STyp, SWymiar> pomocnicza = wsp;

    for(int i = 0; i < SWymiar; i++)
    {
        pomocnicza.wczytajKolumne(i,wyr_wol);
        wx[i] = pomocnicza.det()/wsp.det();
        pomocnicza = wsp;
    }

    return SWektor<STyp,SWymiar>(wx);
}
  //SWektor<STyp, SWymiar> wektorBledu();
 /* double wielkoscBledu()
  {
    SWektor<STyp, SWymiar> wynik = podajWynikCramer();
    SWektor<STyp, SWymiar> blad = (wsp*wynik) - wyr_wol;
    return sqrt(blad*blad);
}*/
};


template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &Strm, SUkladRownanLiniowych<STyp,SWymiar> &UklRown);

template <typename STyp, int SWymiar>
std::ostream& operator << ( std::ostream                  &Strm, 
                            const SUkladRownanLiniowych<STyp,SWymiar>    &UklRown
                          );


#endif
