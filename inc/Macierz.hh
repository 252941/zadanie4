#ifndef MACIERZ_HH
#define MACIERZ_HH
#include "Wektor.hh"
#include "rozmiar.h"
#include <iostream>
#include "LZespolona.hh"


template <typename STyp, int SWymiar>
class SMacierz {
 SWektor<STyp,SWymiar> tab[SWymiar];
 //int indeks=0;
  public:
  SWektor<STyp,SWymiar>& pobierzWiersz(int q)
  {
    return tab[q];
  }
  SWektor<STyp,SWymiar>& pobierzKolumne(int q)
  {
    SWektor<STyp,SWymiar> wynik;
    
    for(int j = 0; j < SWymiar; j++)
    {
        wynik.wczytaj(j, tab[j].pobierz(q) );
    }

    return wynik;
  }
  double& pobierzWyraz(int wiersz, int kolumna)
  {
    return pobierzWiersz(wiersz).pobierz(kolumna);
  }
  void wczytajWiersz(int p, SWektor<STyp,SWymiar> q)
  {
    tab[p]=q;
  }
  void wczytajKolumne(int p, SWektor<STyp,SWymiar> q)
  {
    for(int j = 0; j < ROZMIAR; j++)
    {
        tab[j].wczytaj(p,q.pobierz(j));
    }
  }
  void wczytaj(int wiersz, int kolumna, double wartosc) // wiersz p, kolumna q wartość s
  {
    tab[wiersz].wczytaj(kolumna,wartosc);
  }
  STyp det()
  {
    SMacierz<STyp,SWymiar> mac = (*this);
    STyp x;
    STyp w;

    x = 0;
    w = 1;

    for (int i = 0; i<SWymiar-1; ++i)
    {
        for (int j = i+1; j<SWymiar; ++j)
        {
            x = (mac.tab[j][i])*(-1)/mac.tab[i][i];
            for (int k = i; k<SWymiar; ++k)
            {
                mac.tab[j][k] = mac.tab[j][k] + (x*mac.tab[i][k]);
            }
        }
    }

    for (int l = 0; l<SWymiar; ++l)
    {
        w = (mac(l, l) * w);
    }
    return w;
  }
  //Wektor operator()(Wektor w) { return (*this)*w; }
 // Macierz Macierz::MacierzDopelnien();
 SWektor<STyp,SWymiar>& operator[](int n) { return pobierzWiersz(n); } // pobiera wiersz
 STyp  operator () (unsigned int Ind, unsigned int j) const { return tab[Ind][j]; } //Przeciazenie operatora indeksowania do latwiejszego
    STyp &operator () (unsigned int Ind, unsigned int j)       { return tab[Ind][j]; }
};
template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &wyjscie, SMacierz<STyp,SWymiar> &maci)
{
    for(int j=0;j<SWymiar;j++)
    {
        wyjscie << maci.pobierzWiersz(j) << " ";
    }
    return wyjscie;
}

template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> operator*(SMacierz<STyp,SWymiar> m, SWektor<STyp,SWymiar> w)
{
    SWektor<STyp,SWymiar> wynik;
    for(int i = 0; i < ROZMIAR; i++)
    {
        wynik.wczytaj(i, m.pobierzWiersz(i)*w);
    }
    return wynik;
}
#endif
