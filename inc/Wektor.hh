#ifndef WEKTOR_HH
#define WEKTOR_HH
#include "rozmiar.h"
#include <iostream>


template <typename STyp, int SWymiar>
class SWektor {
  STyp tab[SWymiar];
  public:
  SWektor() = default;
  SWektor(STyp wartosci[])
  {
    for(int j =0; j< SWymiar; j++)
    {
        tab[j] = wartosci[j];
    }
}
  STyp& pobierz(int q) // zapewnienie, że metoda nie zmienia wektora na którym działa
  {
    return tab[q];
  }
  void wczytaj(int p, STyp q)
    {
       tab[p]=q;
    }
  STyp& operator[](int n) { return pobierz(n); }
    
};


//std::istream& operator >> (std::istream &Strm, SWektor &Wek);
template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &Wejscie, SWektor<STyp, SWymiar> &Wek)
{
    STyp m;
    for(int i=0; i<SWymiar; ++i) //Petla potrzebna do zapelnienia komorek tablicy obiektu
    {
        Wejscie >> m;
        Wek.wez(m,i); //Metoda klasy 'wektor' wprowadzajaca wartosci do obiektu
    }
    return Wejscie;
}
//std::ostream& operator << (std::ostream &Strm, SWektor Wek);
template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &wyjscie, SWektor<STyp, SWymiar> wek)
{
    for(int j=0;j<SWymiar;j++)
    {
        wyjscie << wek.pobierz(j) << " ";
    }
    return wyjscie;
}



template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator *(SWektor<STyp, SWymiar> w1, double liczba)
{
    SWektor<STyp, SWymiar> wf;
    for(int j=0;j<SWymiar;j++)
    {
        wf.wczytaj(j,w1.pobierz(j)*liczba);
    }
    return wf;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator /(SWektor<STyp, SWymiar> w1, double liczba)
{
    SWektor<STyp, SWymiar> wf;
    for(int j=0;j<SWymiar;j++)
    {
        wf.wczytaj(j,w1.pobierz(j)/liczba);
    }
    return wf;
}
template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator - (SWektor<STyp, SWymiar> w1, SWektor<STyp, SWymiar> w2)
{
    SWektor<STyp, SWymiar> wf;
    for(int j=0;j<SWymiar;j++)
    {
        wf.wczytaj(j,w1.pobierz(j)-w2.pobierz(j));
    }
    return wf;
}

template <typename STyp, int SWymiar>
SWektor<STyp, SWymiar> operator + (SWektor<STyp, SWymiar> w1, SWektor<STyp, SWymiar> w2)
{
    SWektor<STyp, SWymiar> wf;
    for(int j=0;j<SWymiar;j++)
    {
        wf.wczytaj(j,w1.pobierz(j)+w2.pobierz(j));
    }
    return wf;
}
template <typename STyp, int SWymiar>
STyp operator * (SWektor<STyp, SWymiar> w1, SWektor<STyp, SWymiar> w2)
{
    STyp wynik;
    wynik=0;
    for(int j=0;j<SWymiar;j++)
    {
        wynik+=w1.pobierz(j)*w2.pobierz(j);
    }
    return wynik;
}
#endif
