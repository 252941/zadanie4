#
#  To sa opcje dla kompilacji
#
CPPFLAGS= -c -g -Iinc -Wall -pedantic

__start__: uklad_rownan
	./uklad_rownan

obj:
	mkdir -p obj	

uklad_rownan:  obj/main.o obj/LZespolona.o
	g++ -Wall -pedantic -o uklad_rownan obj/main.o obj/LZespolona.o
        

obj/main.o: src/main.cpp  inc/LZespolona.hh inc/Wektor.hh inc/Macierz.hh\
		inc/UkladRownanLiniowych.hh inc/rozmiar.h
	g++ -c ${CPPFLAGS} -o obj/main.o src/main.cpp


obj/LZespolona.o: src/LZespolona.cpp inc/LZespolona.hh
	g++ -c ${CPPFLAGS} -o obj/LZespolona.o src/LZespolona.cpp

clean:
	rm -f obj/*.o uklad_rownan